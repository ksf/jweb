package com.gdmob.jweb.service;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.gdmob.jweb.common.SplitPage;
public class HomePageService extends BaseService{
  @SuppressWarnings("unused")
  private static Logger log = Logger.getLogger(HomePageService.class);
  public static final HomePageService service = new HomePageService();
  
  public void listArticle(SplitPage splitPage){
		String select = "select id ,title,topimage ";
		splitPageBase(splitPage, select, "pingtai.home.splitPage");
	}
  public void listStar(SplitPage splitPage){
		String select = "select id ,title,topimage ";
		splitPageBase(splitPage, select, "pingtai.home.star");
	}
 
}
