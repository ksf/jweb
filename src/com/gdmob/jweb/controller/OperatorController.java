package com.gdmob.jweb.controller;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Controller;
import com.gdmob.jweb.model.Operator;
import com.gdmob.jweb.service.OperatorService;
import com.gdmob.jweb.validator.OperatorValidator;
import com.jfinal.aop.Before;

@Controller(controllerKey = "/jw/operator")
public class OperatorController extends BaseController {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(OperatorController.class);
	
	private String moduleIds;
	
	public void index() {
		OperatorService.service.list(splitPage);
		render("/pingtai/operator/list.html");
	}

	@Before(OperatorValidator.class)
	public void save() {
		ids = OperatorService.service.save(getModel(Operator.class));
		redirect("/jw/operator");
	}
	
	public void edit() {
		setAttr("operator", OperatorService.service.findOperator(getPara()));
		render("/pingtai/operator/update.html");
	}
	
	@Before(OperatorValidator.class)
	public void update() {
		OperatorService.service.update(getModel(Operator.class));
		redirect("/jw/operator");
	}

	public void view() {
		setAttr("operator",OperatorService.service.findOperator(getPara()));
		render("/pingtai/operator/view.html");
	}
	
	public void delete() {
		OperatorService.service.delete(getPara());
		redirect("/jw/operator");
	}
	
	public void treeData(){
		String json = OperatorService.service.childNodeData(moduleIds);
		renderJson(json);
	}
}


