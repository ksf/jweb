package com.gdmob.jweb.controller;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Controller;
import com.gdmob.jweb.beetl.render.MyCaptchaRender;
import com.jfinal.render.Render;

/**
 * 验证码
 */
@Controller(controllerKey = "/jw/authImg")
public class AuthImgController extends BaseController {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(AuthImgController.class);
	
	public void index() {
		Render render = new MyCaptchaRender();
		render(render);
	}
	
}


