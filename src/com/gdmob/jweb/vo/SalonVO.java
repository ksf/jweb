package com.gdmob.jweb.vo;

public class SalonVO {
	public String ids;
	public String name;
	public String phone;
	public String province;
	public String city;
	public String address;
	public String introduce;
	public String photo;
	public String invitecode;
	public int invitecount;
}
