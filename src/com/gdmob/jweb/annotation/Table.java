package com.gdmob.jweb.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//自定义注解，运行时，可被继承的注解，使用@Table就能够自动写入tableName和默认的主键名字
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface Table {
	
    String tableName();

    String pkName() default "ids";
    
}
