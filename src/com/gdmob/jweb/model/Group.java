package com.gdmob.jweb.model;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Table;
import com.gdmob.jweb.common.DictKeys;
import com.gdmob.jweb.thread.ThreadParamInit;
import com.jfinal.plugin.ehcache.CacheKit;

@SuppressWarnings("unused")
@Table(tableName="jw_group")
public class Group extends BaseModel<Group> {

	private static final long serialVersionUID = 6761767368352810428L;

	private static Logger log = Logger.getLogger(Group.class);
	
	public static final Group dao = new Group();

	/**
	 * 添加或者更新缓存
	 */
	public void cacheAdd(String ids){
		CacheKit.put(DictKeys.cache_name_system, ThreadParamInit.cacheStart_group + ids, Group.dao.findById(ids));
	}

	/**
	 * 删除缓存
	 */
	public void cacheRemove(String ids){
		CacheKit.remove(DictKeys.cache_name_system, ThreadParamInit.cacheStart_group + ids);
	}

	/**
	 * 获取缓存
	 * @param ids
	 * @return
	 */
	public Group cacheGet(String ids){
		Group group = CacheKit.get(DictKeys.cache_name_system, ThreadParamInit.cacheStart_group + ids);
		return group;
	}
	
}
