package com.gdmob.jweb.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Table;
import com.gdmob.jweb.tools.ToolSqlXml;


@SuppressWarnings("unused")
@Table(tableName="jw_metadata")
public class MetaData extends BaseModel<MetaData> {
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(MetaData.class);
	
	public static final MetaData dao = new MetaData();
	public static final String TYPE_COLOR = "color";
	public static final String TYPE_CHAIN = "chain";
	public List<MetaData> getMetaDatasByType(String type){
		String sql = ToolSqlXml.getSql("pingtai.metadata.byType");
		return dao.find(sql,type);
	}
	public MetaData getMetaData(String name,String value,String type){
		String sql = ToolSqlXml.getSql("pingtai.metadata.byValues");
		return dao.findFirst(sql,name,value,type);
	}
}
