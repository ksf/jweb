package com.gdmob.jweb.listener;

import java.io.FileInputStream;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.jfinal.kit.PathKit;
/**
 * 系统启动初始化数据
 */
public class InitListener implements ServletContextListener {
	
	private static Logger log = Logger.getLogger(InitListener.class);
	
	// 销毁
	public void contextDestroyed(ServletContextEvent event) {
		// ServletContext context = event.getServletContext();
	}

	// 启动加载
	public void contextInitialized(ServletContextEvent event) {
		log.info("contextInitialized");
		ServletContext context = event.getServletContext();
		String path = context.getRealPath("/");
		PathKit.setWebRootPath(path);
		String file = context.getInitParameter("log4j_init_path");   
   		String logFile = context.getInitParameter("log4j_file_path");   
   		if(file != null){   
   			try{   
   	   			Properties prop = new Properties();
   				prop.load(new FileInputStream(path+file)); //加载log4j.properties
   				prop.setProperty("log4j.appender.F1.File", path + logFile + prop.getProperty("log4j.appender.F1.File")); //设置日志文件的输出路径
   				prop.setProperty("log4j.appender.F2.File", path + logFile + prop.getProperty("log4j.appender.F2.File")); 
   				PropertyConfigurator.configure(prop); //加载配置项
   			}catch(Exception e){
   				log.info("初始化log4j日志输入路径异常，请检查web.xml参数配置是否正常，异常发生在" + this.getClass().getName() + "类的public void init()方法，异常的愿意是：" + e.getMessage(), e.fillInStackTrace());   
   			}
   		}
	}

}
